/*
 * Virtio VIO driver
 *
 * This module allows virtio devices to be used over a virtual VIO device.
 * This can be used with PowerVM based VMMs
 *
 * Copyright IBM Corp. 2010
 *
 * Authors:
 *  Erlon R. Cruz <erlon.cruz@br.flextronics.com>
 *  Pedro Scarappicha <pedro.scarappicha@br.flextronics.com>
 *  Ricardo M. Matinata  <matinata@br.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2 or later.
 * See the COPYING file in the top-level directory.
 *
 */

#ifndef VIRTIO_VIO_H
#define VIRTIO_VIO_H

#include <linux/types.h>
#include <linux/virtio_ids.h>
#include <linux/virtio_config.h>
#include <linux/virtio_net.h>
#include <linux/virtio_blk.h>

#define VIRTIO_BLK_F_WCACHE 9


/* *
 * The shared buffer between client and user space
 *
 *
 * swap_buff layout:                   d = c + TLB_LEN
 * .--------------------------------------------------.
 * | CMD_BUF_LEN | RING_MAX_LEN |  RNG_DBUF_LEN       |
 * .--------------------------------------------------.
 * ^           ^                ^                     ^
 * a=0    b=RING_OFFSET   c=RING_DATABUF_OFFSET   d=SWAP_SIZE
 *
 * a = Command buffer. Used to swap commands between user and kernel space
 * b = vring buffer.
 * c = data buffer. Data refered in vring points(in terms of offsets
 *	from the start c, i.e., a buffer placed in c has offset 0 and so on)
 *
 */
#define MAX_SG_UNIT 4096
#define VRING_ELEMENTS 128
#define POOL_LEN (MAX_SG_UNIT*VRING_ELEMENTS) /* memory reserved to map SGs */
#define CMD_BUF_LEN (MAX_SG_UNIT*10) /* Area to send/receid data and command to userspace */
#define RING_OFFSET (0 + CMD_BUF_LEN) /* vring buffer start */
#define RING_MAX_LEN (vring_size(VRING_ELEMENTS, VIRTIO_PCI_VRING_ALIGN))
#define RING_DATABUF_OFFSET (RING_OFFSET + RING_MAX_LEN)
#define RNG_DBUF_LEN (POOL_LEN)
#define SWAP_SIZE (CMD_BUF_LEN+RING_MAX_LEN+RNG_DBUF_LEN)

#define DATAREADY 1		//The user sinalizes he changed the shared buffer
#define DATAREAD 2		//The user sinalizes he read the shared buffer

#define VV_GET_CONFIGS					1
#define VV_GET_CONFIGS_RESPONSE				2
#define VV_GET_CONFIG_HDR				3
#define VV_GET_CONFIG_HDR_RESPONSE			4
#define VV_REFRESH_CONFIG_HDR                           5
#define VV_REFRESH_CONFIG_HDR_RESPONSE			6
#define VV_REFRESH_CONFIG_SPACE                         7
#define VV_REFRESH_CONFIG_SPACE_RESPONSE		8
#define VV_FIND_VQ					9
#define VV_FIND_VQ_RESPONSE				10
#define VV_FIND_VQ_RSP_FAIL				11
#define VV_DEL_VQ					12
#define VV_DEL_VQ_RESPONSE				13
#define VV_RING_KICK					15
#define VV_NOTIFY					16
#define VV_NOTIFY_RESPONSE				17
#define VV_KICK_DONE					18
#define VV_KICK_ERROR					19
#define VV_SERVER_RESET					20
#define VV_SERVER_RESET_DONE				21
#define VV_KICK_BUSY					22

struct vv_device_header {
	__u8 type;
	__u8 num_vqs;
	__u8 vqs_size;
	__u32 device_features;
	__u32 guest_features;
	__u8 config_len;
	__u8 device_status;
	__u8 config[0];
} __attribute__ ((packed));

struct swap_buff {
	__u8 qemu_cmd;		//kernel->qemu
	__u8 qemu_rsp;		//kernel<-qemu
	__u32 in_param;		//kernel->qemu
	__u32 out_param;	//kernel<-qemu
	__u16 len;
	char data[0];
} __attribute__ ((packed));

#endif //VIRTIO_VIO_H
