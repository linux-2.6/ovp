/*
 * Virtio VIO driver
 *
 * This module allows virtio devices to be used over a virtual VIO device.
 * This can be used with PowerVM based VMMs
 *
 * Copyright IBM Corp. 2010
 *
 * Authors:
 *  Erlon R. Cruz <erlon.cruz@br.flextronics.com>
 *  Pedro Scarappicha <pedro.scarappicha@br.flextronics.com>
 *  Ricardo M. Matinata  <matinata@br.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2 or later.
 * See the COPYING file in the top-level directory.
 *
 */

#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/virtio.h>
#include <linux/virtio_pci.h>
#include <linux/virtio_ring.h>
#include <linux/virtio_config.h>
#include <linux/virtio_ids.h>
#include <asm/hvcall.h>
#include <asm/iommu.h>
#include <asm/prom.h>
#include <asm/param.h>
#include <asm/dma-mapping.h>

#include <linux/delay.h>
#include <asm/vio.h>
#include "virtio_vio.h"

#define BUFF_SIZE PAGE_SIZE

#define to_vvq(_vq) container_of(_vq, struct vring_virtqueue, vq)

MODULE_DESCRIPTION("virtio-vio");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");

static struct ibmvirtio_ops *ibmvirtio_ops;

static void data_send(struct virtio_vio_device *virtiodata, u8 cfunction,
		      u32 param, u32 * buf, u16 buff_len)
{
	struct vv_crq crq;
	u64 *crq_as_u64 = (u64 *) & crq;
	int srv_retries = 5;

	crq.valid = 0x80;
	crq.format = cfunction;
	crq.param = param;
	crq.IU_length = buff_len;

	switch (cfunction) {
	case VV_REFRESH_CONFIG_HDR:
	case VV_GET_CONFIG_HDR:
		crq.IU_length = sizeof(struct vv_device_header);
		memcpy(&crq.IU_data_ptr, &virtiodata->vdata.devhdr_token, 8);
		break;
	case VV_REFRESH_CONFIG_SPACE:
	case VV_GET_CONFIGS:
		crq.IU_length = virtiodata->vdata.devhdr->config_len;
		memcpy(&crq.IU_data_ptr, &virtiodata->vdata.devcfg_token, 8);
		break;
	case VV_FIND_VQ:
		memcpy(&crq.IU_data_ptr, buf, 8);
		break;
	}

	virtiodata->response_received = 0;
	ibmvirtio_ops->send_crq(virtiodata, crq_as_u64[0], crq_as_u64[1]);

	//hold execution until receives server response
	do {
		wait_event_interruptible_timeout(virtiodata->sender_queue,
						 (virtiodata->
						  response_received != 0),
						 HZ * 1);
		if (!virtiodata->response_received) {
			printk("virtio_vioc: waiting for qemu-vio...(%d)\n",
			       srv_retries);
			srv_retries--;
		}

		if (srv_retries <= 0) {
			printk("virtio_vioc: server failure!\n");
			virtiodata->response_received = -1;
			return;
		}
		//TODO: Cont nn times and give up
	} while (!virtiodata->response_received);

}

static void data_send_no_wait(struct virtio_vio_device *virtiodata,
			      u8 cfunction, u32 param, u32 * buf, u16 buff_len)
{
	struct vv_crq crq;
	u64 *crq_as_u64 = (u64 *) & crq;

	crq.valid = 0x80;
	crq.format = cfunction;
	crq.param = param;
	crq.IU_length = buff_len;

	switch (cfunction) {
	case VV_REFRESH_CONFIG_HDR:
	case VV_GET_CONFIG_HDR:
		crq.IU_length = sizeof(struct vv_device_header);
		memcpy(&crq.IU_data_ptr, &virtiodata->vdata.devhdr_token, 8);
		break;
	case VV_REFRESH_CONFIG_SPACE:
	case VV_GET_CONFIGS:
		crq.IU_length = virtiodata->vdata.devhdr->config_len;
		memcpy(&crq.IU_data_ptr, &virtiodata->vdata.devcfg_token, 8);
		break;
	case VV_FIND_VQ:
		memcpy(&crq.IU_data_ptr, buf, 8);
		break;
	}

	virtiodata->response_received = 0;
	ibmvirtio_ops->send_crq(virtiodata, crq_as_u64[0], crq_as_u64[1]);
}

static void refresh_config_hdr(struct virtio_vio_device *virtiodata)
{
	data_send(virtiodata, VV_REFRESH_CONFIG_HDR, 0, 0, 0);
}

static void refresh_config_space(struct virtio_vio_device *virtiodata)
{
	data_send(virtiodata, VV_REFRESH_CONFIG_SPACE, 0, 0, 0);
}

static u32 vv_get_features(struct virtio_device *vdev)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_device_header *hdr = virtiodata->vdata.devhdr;

	return hdr->device_features;
}

static void vv_finalize_features(struct virtio_device *vdev)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_device_header *hdr = virtiodata->vdata.devhdr;

	hdr->device_features = vdev->features[0];
	refresh_config_hdr(virtiodata);
}

static void vv_get(struct virtio_device *vdev, unsigned offset,
		   void *buf, unsigned len)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct virtio_blk_config *cfg =
	    (struct virtio_blk_config *)virtiodata->vdata.devcfg;

	memcpy(buf, (char *)cfg + offset, len);
}

static void vv_set(struct virtio_device *vdev, unsigned offset,
		   const void *buf, unsigned len)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct virtio_blk_config *cfg =
	    (struct virtio_blk_config *)virtiodata->vdata.devcfg;

	memcpy((char *)cfg + offset, buf, len);
	refresh_config_space(virtiodata);
}

static u8 vv_get_status(struct virtio_device *vdev)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_device_header *hdr = virtiodata->vdata.devhdr;

	return hdr->device_status;
}

static void vv_set_status(struct virtio_device *vdev, u8 status)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_device_header *hdr = virtiodata->vdata.devhdr;

	hdr->device_status = status;
	refresh_config_hdr(virtiodata);
}

static void vv_reset(struct virtio_device *vdev)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_device_header *hdr = virtiodata->vdata.devhdr;

	hdr->device_status = 0;
	refresh_config_hdr(virtiodata);
}

static void vv_notify(struct virtqueue *vq)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vq->vdev->dev);
	unsigned int id = virtiodata->kick_sent++;
	struct vring_virtqueue *vvq = to_vvq(vq);

	if (vq->busy) {
		pdebug("%s: kick %d blocked\n", __func__, id);
		return;
	}
	if (!(vvq->vring.avail->idx - vvq->vring.used->idx)) {
		pdebug("%s: kick %d skipped\n", __func__, id);
		return;
	}
	vq->busy = 1;
	pdebug("virtio_vioc: ==== sent kick %d (%d elements) ====\n",
	       id, (vvq->vring.avail->idx - vvq->vring.used->idx));

	data_send_no_wait(virtiodata, VV_NOTIFY, id, 0, 0);

}

static void vv_del_vq(struct virtio_device *vdev, struct vv_ring_info *vrinf)
{
	unsigned long flags;
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);

	data_send(virtiodata, VV_DEL_VQ, vrinf->queue_index, 0, 0);
	printk("virtio_vioc: removed %d elements vring idx %d\n", vrinf->num,
	       vrinf->queue_index);

	spin_lock_irqsave(&virtiodata->vdata.lock, flags);
	list_del(&vrinf->node);
	spin_unlock_irqrestore(&virtiodata->vdata.lock, flags);

	vring_del_virtqueue(vrinf->vq);
	dma_free_coherent(virtiodata->dev,
			  vring_size(vrinf->num, VIRTIO_PCI_VRING_ALIGN),
			  vrinf->queue, vrinf->queue_token);
	kfree(vrinf);
}

static void vv_del_vqs(struct virtio_device *vdev)
{
	struct vv_ring_info *vrinf, *n;
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);

	list_for_each_entry_safe(vrinf, n, &virtiodata->vdata.vqs_infos, node) {
		if (vrinf) {
			vv_del_vq(vdev, vrinf);
		}
	}

}

static struct virtqueue *find_vq(struct virtio_device *vdev,
				 unsigned index,
				 void (*callback) (struct virtqueue * vq),
				 const char *name)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	struct vv_ring_info *vring_inf;
	unsigned long int flags;
	struct virtqueue *rc;

	if (index >= virtiodata->vdata.devhdr->num_vqs)
		return ERR_PTR(-ENOENT);

	vring_inf = kmalloc(sizeof(struct vv_ring_info), GFP_KERNEL);
	if (!vring_inf) {
		dev_err(virtiodata->dev,
			"virtio_vioc: can't get a free page (vring_inf)\n");
		return ERR_PTR(-ENOMEM);
	}

	vring_inf->num = virtiodata->vdata.devhdr->vqs_size;
	vring_inf->queue_index = index;
	vring_inf->queue = dma_alloc_coherent(virtiodata->dev,
					      vring_size(vring_inf->num,
							 VIRTIO_PCI_VRING_ALIGN),
					      &vring_inf->queue_token, 0);
	if (!vring_inf->queue) {
		printk
		    ("virtio_vioc: can't get a free page (vring_inf->queue)\n");
		rc = ERR_PTR(-ENOMEM);
		goto err_dma_alloc_coherent;
	}
	memset(vring_inf->queue, 0x00,
	       vring_size(vring_inf->num, VIRTIO_PCI_VRING_ALIGN));

	vring_inf->vq =
	    vring_new_virtqueue(vring_inf->num, VIRTIO_PCI_VRING_ALIGN, vdev,
				(void *)vring_inf->queue, vv_notify, callback,
				name);
	if (!vring_inf->vq) {
		rc = ERR_PTR(-ENOMEM);
		goto err_vring_new_virtqueue;
	}
	//Notify server about new vq. Wait here for server ack
	data_send(virtiodata, VV_FIND_VQ, vring_inf->queue_index,
		  (u32 *) & vring_inf->queue_token, vring_inf->num);
	if (virtiodata->response_received == VV_FIND_VQ_RSP_FAIL) {
		printk("virtio_vioc: %s: server couldn't alloc vqueue\n",
		       __func__);
		rc = ERR_PTR(-ENOMEM);
		goto err_vring_new_virtqueue;
	}

	printk("virtio_vioc: added  %d elements vring idx %d\n", vring_inf->num,
	       vring_inf->queue_index);
	spin_lock_irqsave(&virtiodata->vdata.lock, flags);
	list_add(&vring_inf->node, &virtiodata->vdata.vqs_infos);
	spin_unlock_irqrestore(&virtiodata->vdata.lock, flags);

	return vring_inf->vq;

      err_vring_new_virtqueue:
	dma_free_coherent(virtiodata->dev,
			  vring_size(vring_inf->num, VIRTIO_PCI_VRING_ALIGN),
			  vring_inf->queue, vring_inf->queue_token);
      err_dma_alloc_coherent:
	kfree(vring_inf);
	return rc;

}

/* the config->find_vqs() implementation */
static int vv_find_vqs(struct virtio_device *vdev, unsigned nvqs,
		       struct virtqueue *vqs[],
		       vq_callback_t * callbacks[], const char *names[])
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);
	int i, err = 0;

	/* We must have this many virtqueues. */
	if (nvqs > virtiodata->vdata.devhdr->num_vqs) {
		printk("virtio_vioc: host doesnt support this much(%d) vqs\n",
		       nvqs);
		return -ENOENT;
	}

	for (i = 0; i < nvqs; ++i) {
		vqs[i] = find_vq(vdev, i, callbacks[i], names[i]);
		if (IS_ERR(vqs[i])) {
			err = PTR_ERR(vqs[i]);
			goto error_find;
		}
	}

	return 0;

      error_find:
	vv_del_vqs(vdev);
	return err;

}

void vv_handle_crq(struct vv_crq *crq, struct virtio_vio_device *virtiodata)
{
	int rc;
	struct vv_ring_info *vrinf, *n;
	struct vring_virtqueue *vvq = NULL;

	switch (crq->valid) {
	case 0xC0:
		switch (crq->format) {
		case 0x01:	/* Initialization message */
			rc = ibmvirtio_ops->send_crq(virtiodata,
						     0xC002000000000000LL, 0);
			virtiodata->partner_ready = 1;
			break;

		case 0x02:	/* Initialization response */
			printk("virtio_vioc: partner initialized\n");
			virtiodata->partner_ready = 1;
			wake_up_interruptible(&virtiodata->sender_queue);
			break;
		default:
			printk
			    ("virtio_vioc: unknown crq message format type: %d\n",
			     crq->format);
			break;
		}
		break;

	case 0xFF:		/* Hypervisor telling us the connection is closed */
		printk("virtio_vioc: connection closed\n");
		virtiodata->partner_ready = 0;

		break;

	case 0x80:

		virtiodata->response_received = crq->format;
		wake_up_interruptible(&virtiodata->sender_queue);

		switch (crq->format) {

		case VV_GET_CONFIGS_RESPONSE:
		case VV_GET_CONFIG_HDR_RESPONSE:
		case VV_REFRESH_CONFIG_HDR_RESPONSE:
		case VV_REFRESH_CONFIG_SPACE_RESPONSE:
		case VV_FIND_VQ_RESPONSE:
		case VV_DEL_VQ_RESPONSE:
		case VV_FIND_VQ_RSP_FAIL:
			break;
		case VV_KICK_DONE:
			pdebug
			    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: return from kick %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
			     crq->param);

			/* TODO: Still need to handle multiple vqs */
			list_for_each_entry_safe(vrinf, n,
						 &virtiodata->vdata.vqs_infos,
						 node) {

				vvq = to_vvq(vrinf->vq);

				if (vvq)
					vvq->vq.busy = 0;

				if (vrinf->vq->callback)
					vrinf->vq->callback(vrinf->vq);
			}
			pdebug
			    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: return from kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
			     crq->param);
			break;
		case VV_KICK_ERROR:
			pdebug
			    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: error from kick %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
			     crq->param);

			list_for_each_entry_safe(vrinf, n,
						 &virtiodata->vdata.vqs_infos,
						 node) {
				vvq = to_vvq(vrinf->vq);
				if (vvq)
					vvq->vq.busy = 0;
				if (vrinf->vq->callback)
					vrinf->vq->callback(vrinf->vq);
			}

			pdebug
			    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: error from kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
			     crq->param);
			break;
		case VV_KICK_BUSY:
			pdebug
			    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: busy from kick %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
			     crq->param);

			list_for_each_entry_safe(vrinf, n,
						 &virtiodata->vdata.vqs_infos,
						 node) {

				vvq = to_vvq(vrinf->vq);
				vvq->vq.busy = 1;
				if (vrinf->vq->callback)
					vrinf->vq->callback(vrinf->vq);
			}
			pdebug
			    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: busy from kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
			     crq->param);
			break;
		default:
			printk
			    ("virtio_vioc: unknown crq message format type: %d\n",
			     crq->format);
			break;

		}

		break;
	default:
		printk("virtio_vios: Invalid CRQ\n");
		break;
	}
	return;
}

static struct virtio_config_ops virtio_config_ops = {
	.get = vv_get,
	.set = vv_set,
	.get_status = vv_get_status,
	.set_status = vv_set_status,
	.reset = vv_reset,
	.find_vqs = vv_find_vqs,
	.del_vqs = vv_del_vqs,
	.get_features = vv_get_features,
	.finalize_features = vv_finalize_features,
};

static void virtio_release_dev(struct device *_d)
{
	;
}

static void init_virtio_device(struct virtio_vio_device *virtiodata,
			       u8 device_type)
{
	virtiodata->vdata.vd.dev.parent = virtiodata->dev;
	virtiodata->vdata.vd.dev.release = virtio_release_dev;
	virtiodata->vdata.vd.config = &virtio_config_ops;
	virtiodata->vdata.vd.id.vendor = VIRTIO_DEV_ANY_ID;
	virtiodata->vdata.vd.id.device = device_type;	//net or block
	virtiodata->vdata.vd.priv = NULL;	//We can not use this.
	//Used by virtio_blk/virtio_net
	dev_set_drvdata(&virtiodata->vdata.vd.dev, virtiodata);

	INIT_LIST_HEAD(&virtiodata->vdata.vd.vqs);
	INIT_LIST_HEAD(&virtiodata->vdata.vqs_infos);
	spin_lock_init(&virtiodata->vdata.lock);
}

void virtio_vio_bind()
{
}

EXPORT_SYMBOL_GPL(virtio_vio_bind);

/* VIO probing function*/
static int __devinit virtio_vio_probe(struct vio_dev *vdev,
				      const struct vio_device_id *id)
{
	struct virtio_vio_device *virtiodata;
	int rc = 0;

	virtiodata = kmalloc(sizeof(struct virtio_vio_device), GFP_KERNEL);
	if (!virtiodata) {
		dev_err(&vdev->dev,
			"virtio_vioc: can't get a free page (virtiodata)\n");
		return -ENOMEM;
	}
	memset(virtiodata, 0x00, sizeof(struct virtio_vio_device));

	virtiodata->dev = &vdev->dev;
	virtiodata->partner_ready = 0;
	virtiodata->response_received = 0;
	virtiodata->kick_sent = 0;
	init_waitqueue_head(&virtiodata->sender_queue);
	dev_set_drvdata(&vdev->dev, virtiodata);

	rc = ibmvirtio_ops->init_crq_queue(&virtiodata->queue, virtiodata, 0);
	if (rc != 0 && rc != H_RESOURCE) {
		dev_err(&vdev->dev, "virtio_vioc: couldn't initialize crq\n");
		rc = -ENOMEM;
		goto err_init_crq;
	}
	// send the initial CRQ message
	rc = ibmvirtio_ops->send_crq(virtiodata, 0xC001000000000000LL, 0);

	wait_event_interruptible_timeout(virtiodata->sender_queue,
					 (virtiodata->partner_ready == 1),
					 HZ * 5);
	if (!virtiodata->partner_ready) {
		printk
		    ("virtio_vioc: server timeout! Aborting module probing...\n");
		rc = -ERESTARTSYS;
		goto err_server_timeout;
	}
	//Request device header
	virtiodata->vdata.devhdr = dma_alloc_coherent(virtiodata->dev,
						      sizeof(struct
							     vv_device_header),
						      &virtiodata->vdata.
						      devhdr_token, GFP_KERNEL);
	if (!virtiodata->vdata.devhdr) {
		printk
		    ("virtio_vioc: can't get a free page (virtiodata->vdata.dev_config)\n");
		rc = -ENOMEM;
		goto err_server_timeout;
	}

	data_send(virtiodata, VV_GET_CONFIG_HDR, 0, 0, 0);
	if (virtiodata->response_received != VV_GET_CONFIG_HDR_RESPONSE) {
		printk
		    ("virtio_vioc: server timeout! Aborting module probing...\n");
		rc = -ERESTARTSYS;
		goto err_get_hdr;
	}
	//Alloc config space
	virtiodata->vdata.devcfg = dma_alloc_coherent(virtiodata->dev,
						      virtiodata->vdata.devhdr->
						      config_len,
						      &virtiodata->vdata.
						      devcfg_token, GFP_KERNEL);
	if (!virtiodata->vdata.devcfg) {
		printk
		    ("virtio_vioc: can't get a free page (virtiodata->vdata.dev_config)\n");
		rc = -ENOMEM;
		goto err_get_hdr;
	}

	data_send(virtiodata, VV_GET_CONFIGS, 0, 0, 0);
	if (virtiodata->response_received != VV_GET_CONFIGS_RESPONSE) {
		printk
		    ("virtio_vioc: server timeout! Aborting module probing...\n");
		rc = -ERESTARTSYS;
		goto err_get_cfg;
	}

	init_virtio_device(virtiodata, virtiodata->vdata.devhdr->type);

	rc = register_virtio_device(&virtiodata->vdata.vd);
	if (rc) {

		printk("virtio_vioc: can't register virtio device\n");
		return -ENOMEM;
	}

	return 0;

      err_get_cfg:
	dma_free_coherent(virtiodata->dev, virtiodata->vdata.devhdr->config_len,
			  virtiodata->vdata.devcfg,
			  virtiodata->vdata.devcfg_token);
      err_get_hdr:
	dma_free_coherent(virtiodata->dev, sizeof(struct vv_device_header),
			  virtiodata->vdata.devhdr,
			  virtiodata->vdata.devhdr_token);
      err_server_timeout:
	ibmvirtio_ops->release_crq_queue(&virtiodata->queue, virtiodata, 0);
      err_init_crq:
	kfree(virtiodata);

	return rc;
}

static int __devexit virtio_vio_remove(struct vio_dev *vdev)
{
	struct virtio_vio_device *virtiodata = dev_get_drvdata(&vdev->dev);

	unregister_virtio_device(&virtiodata->vdata.vd);

	dma_free_coherent(virtiodata->dev, virtiodata->vdata.devhdr->config_len,
			  virtiodata->vdata.devcfg,
			  virtiodata->vdata.devcfg_token);

	dma_free_coherent(virtiodata->dev, sizeof(struct vv_device_header),
			  virtiodata->vdata.devhdr,
			  virtiodata->vdata.devhdr_token);

	ibmvirtio_ops->release_crq_queue(&virtiodata->queue, virtiodata, 0);

	kfree(virtiodata);
	return 0;
}

static struct vio_device_id virtio_vio_id_table[] = {
	{"vscsi", "IBM,v-scsi"},
	{"", ""}
};

MODULE_DEVICE_TABLE(vio, virtio_vio_id_table);

static struct vio_driver virtio_vio_driver = {
	.id_table = virtio_vio_id_table,
	.probe = virtio_vio_probe,
	.remove = virtio_vio_remove,
	.driver = {
		   .name = "virtio_vioc",
		   .owner = THIS_MODULE,
		   }
};

static int __init virtio_vio_init(void)
{
	printk("virtio_vioc: init...\n");

	ibmvirtio_ops = &vv_ops;
	return vio_register_driver(&virtio_vio_driver);
}

static void __exit virtio_vio_exit(void)
{
	printk("virtio_vioc: ...exit.\n");
	vio_unregister_driver(&virtio_vio_driver);
}

module_init(virtio_vio_init);
module_exit(virtio_vio_exit);
