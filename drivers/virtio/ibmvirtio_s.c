/*
 * Virtio VIO driver
 *
 * This module allows virtio devices to be used over a virtual VIO device.
 * This can be used with PowerVM based VMMs
 *
 * Copyright IBM Corp. 2010
 *
 * Authors:
 *  Erlon R. Cruz <erlon.cruz@br.flextronics.com>
 *  Pedro Scarappicha <pedro.scarappicha@br.flextronics.com>
 *  Ricardo M. Matinata  <matinata@br.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2 or later.
 * See the COPYING file in the top-level directory.
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/pm.h>
#include <asm/firmware.h>
#include <asm/vio.h>
#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <linux/wait.h>
#include <linux/semaphore.h>
#include <linux/virtio_vio.h>
#include <linux/virtio_ring.h>
#include <linux/virtio_pci.h>
#include <linux/list.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/time.h>

#include <linux/sched.h>
#include <linux/virtio_blk.h>

#include "virtio_vio.h"

#define DEV_NAME "virtio"
#define DATAREADY 1		
#define MAX_ADAPTERS 20

#define DRIVER_VERSION "1.0"
#define h_copy_rdma(l, sa, sb, da, db) \
            plpar_hcall_norets(H_COPY_RDMA, l, sa, sb, da, db)

MODULE_LICENSE("GPL");

static struct ibmvirtio_ops *ibmvirtio_ops;

struct cdev_index cdevindex[MAX_ADAPTERS];//Up to 20 vscsi adapters in server
int devindex = 0, cdev_major;
struct semaphore idxsem;

/***********************************************************************
 * Vring and vqueue management
 **********************************************************************/

static int send_ack(struct virtio_vio_device *virtiodata, int ack_num,
		    int param)
{

	struct vv_crq crq;
	u64 *crq_as_u64 = (u64 *) & crq;
	memset(&crq, 0x00, sizeof(crq));

	crq.valid = 0x80;
	crq.format = ack_num;
	crq.param = param;

	ibmvirtio_ops->send_crq(virtiodata, crq_as_u64[0], crq_as_u64[1]);
	return 0;
}

static void server_reset(struct virtio_vio_device *virtiodata)
{

	unsigned long int flags, vring_len;
	struct vv_ring_info *vrinf, *n;

	printk("virtio_vios: reseting server\n");

	list_for_each_entry_safe(vrinf, n, &virtiodata->srv_vqs, node) {
		printk("virtio_vios: removed %d elements vring idx %d\n",
		       vrinf->num, vrinf->queue_index);
		spin_lock_irqsave(&virtiodata->vqs_lock, flags);
		list_del(&vrinf->node);
		spin_unlock_irqrestore(&virtiodata->vqs_lock, flags);
		vring_len = vring_size(vrinf->num, VIRTIO_PCI_VRING_ALIGN);
		memset(vrinf->queue, 0x00, vring_len);
		kfree(vrinf);
	}

	memset(virtiodata->outbuffer, 0x00, SWAP_SIZE);
	memset(virtiodata->rngcpy, 0x00, RING_MAX_LEN);
	virtiodata->last_proc_idx = 0;
	virtiodata->partner_ready = 0;
	virtiodata->vqs_count = 0;
	virtiodata->current_pos = 0;
	virtiodata->vring_busy = 0;
}

static int server_add_vq(struct virtio_vio_device *virtiodata,
			 struct vv_crq *crq)
{

	unsigned long int flags;
	struct vv_ring_info *vqinfo;
	char *server_ring;
	unsigned int vring_len;
	dma_addr_t server_ring_tk;

	vqinfo = kmalloc(sizeof(struct vv_ring_info), GFP_KERNEL);
	if (!vqinfo) {
		printk("virtio_vios: %s: error allocating memmory\n", __func__);
		return -1;
	}

	vqinfo->vq = NULL;
	vqinfo->num = crq->IU_length;
	vqinfo->queue_index = crq->param;
	vqinfo->queue_token = crq->IU_data_ptr;

	server_ring = (char *)virtiodata->outbuffer + RING_OFFSET;
	server_ring_tk = virtiodata->outbuffer_token + RING_OFFSET;
	vring_len = vring_size(vqinfo->num, VIRTIO_PCI_VRING_ALIGN);

	if (vring_len > RING_MAX_LEN) {

		printk
		    ("virtio_vios: %s: ring too big(%d bytes) - availiable size = %d\n",
		     __func__, vring_len, RING_MAX_LEN);
		return -1;
	}
	vqinfo->queue = server_ring;

	memset(server_ring, 0x00, vring_len);

	spin_lock_irqsave(&virtiodata->vqs_lock, flags);
	list_add(&vqinfo->node, &virtiodata->srv_vqs);
	virtiodata->vqs_count++;
	spin_unlock_irqrestore(&virtiodata->vqs_lock, flags);

	printk
	    ("virtio_vios: added  %d elements vring idx %d. %d vqs into server\n",
	     vqinfo->num, vqinfo->queue_index, virtiodata->vqs_count);

	return 0;
}

static int server_del_vq(struct virtio_vio_device *virtiodata, int index)
{

	unsigned long int flags, vring_len;
	struct vv_ring_info *vrinf, *n;

	list_for_each_entry_safe(vrinf, n, &virtiodata->srv_vqs, node) {
		if (vrinf->queue_index == index) {
			printk
			    ("virtio_vios: removed %d elements vring idx %d\n",
			     vrinf->num, vrinf->queue_index);
			spin_lock_irqsave(&virtiodata->vqs_lock, flags);
			list_del(&vrinf->node);
			spin_unlock_irqrestore(&virtiodata->vqs_lock, flags);
			vring_len =
			    vring_size(vrinf->num, VIRTIO_PCI_VRING_ALIGN);
			memset(vrinf->queue, 0x00, vring_len);
			virtiodata->vring_busy = 0;
			kfree(vrinf);
			return 0;
		}
		return -ENOENT;
	}

	return 0;
}

static inline int more_reqs(struct virtio_vio_device *virtiodata){

    char *server_ring;
    struct vring vr;

    server_ring = (char *)virtiodata->outbuffer + RING_OFFSET;
    vring_init(&vr, VRING_ELEMENTS, server_ring, VIRTIO_PCI_VRING_ALIGN);

    return vr.avail->idx - vr.used->idx;
}

/*
 * Maps the vring SG slocally
 */
int process_desc(struct virtio_vio_device *virtiodata,
			struct vring *rng,
			struct vring *rngcpy,
			int todo_items, int first, unsigned int id)
{
	struct vring_used *us;
	struct vring_avail *av;
	struct vring_desc *desc, *cur;
        int next_desc_idx, i, rc, offset = 0;

	dma_addr_t desc_data_tk =
	    virtiodata->outbuffer_token + RING_DATABUF_OFFSET;
	
	us = rngcpy->used;
	av = rngcpy->avail;
	desc = rngcpy->desc;

	pdebug("virtio_vios: ==== %s: processing %d requests in kick %d ====\n",
		    __func__,todo_items,id);

	for (i = first; todo_items > 0; todo_items--, i = (i + 1) % rngcpy->num) {
		cur = &desc[av->ring[i]];
		next_desc_idx = av->ring[i];
		do {

			cur->offset = offset;

			if (!(cur->flags & VRING_DESC_F_WRITE)) {

				rc = h_copy_rdma(cur->len,
						 virtiodata->riobn,
						 cur->addr,
						 virtiodata->liobn,
						 desc_data_tk + offset);

				if (rc != H_SUCCESS) {

					printk
					    ("virtio_vios: %s: transferring data error nro %d. tce=%llu, offset=%d\n",
					     __func__, rc, cur->addr, offset);
					return rc;
				}
			}

			offset += cur->len;

			/*Copy the new entry to the shared vring so qemu can see the changes*/
			memcpy(&rng->desc[next_desc_idx], cur,
			       sizeof(struct vring_desc));

			if (!(cur->flags & VRING_DESC_F_NEXT)) {
				break;
			}
			next_desc_idx = cur->next;
			cur = &desc[cur->next];

		} while (1);

	}

	return 0;

}

int handle_kick(struct virtio_vio_device *virtiodata, unsigned int id)
{
	int num = 0, rc = 0, todo_items, first;
	struct vv_ring_info *vrinf, *n;
	unsigned int *queue = NULL, qlen = 0, copy_len;
	char *server_ring;
	dma_addr_t queue_token = 0, server_ring_tk;
	struct vring vr, vrcpy;
	unsigned long flags;

	server_ring = (char *)virtiodata->outbuffer + RING_OFFSET;
	server_ring_tk = virtiodata->outbuffer_token + RING_OFFSET;

	list_for_each_entry_safe(vrinf, n, &virtiodata->srv_vqs, node) {
		queue_token = vrinf->queue_token;
		queue = vrinf->queue;
		qlen = vring_size(vrinf->num, VIRTIO_PCI_VRING_ALIGN);
		num = vrinf->num;
	}

	// initialize the ring
	vring_init(&vr, num, server_ring, VIRTIO_PCI_VRING_ALIGN);
	vring_init(&vrcpy, num, virtiodata->rngcpy, VIRTIO_PCI_VRING_ALIGN);

	//Copy only desc and used table
	copy_len = ((sizeof(struct vring_desc) * num + sizeof(__u16) * (2 + num)
		     + VIRTIO_PCI_VRING_ALIGN - 1) & ~(VIRTIO_PCI_VRING_ALIGN -
						       1));

//	spin_lock_irqsave(&virtiodata->vring_lock, flags);

	/*
	 * Make a copy of the client vring into a private area, so nobody bother
	 * while the SGs is not finished.
	 */
	rc = h_copy_rdma(copy_len,
			 virtiodata->riobn,
			 queue_token,
			 virtiodata->liobn, virtiodata->rngcpy_token);
	if (rc != H_SUCCESS) {
		printk("virtio_vios: %s: %d transferring data error\n",
		       __func__, rc);
//		spin_unlock_irqrestore(&virtiodata->vring_lock, flags);
		return rc;
	}

	/*
	 * The number of items that are not processed in the ring
	 */
	todo_items = vrcpy.avail->idx - virtiodata->last_proc_idx;
	if (todo_items < 0)
		todo_items =
		    (vrcpy.avail->idx + 65536) - virtiodata->last_proc_idx;
	first = virtiodata->last_proc_idx % vrcpy.num;
	virtiodata->last_proc_idx = vrcpy.avail->idx;

	spin_unlock_irqrestore(&virtiodata->vring_lock, flags);

	/*
	 * Some time we got kicked with no elements to process
	 */
	if (todo_items == 0) {
		return -2;
	}

	/*
	 * Maps the vring SG slocally
	 */
	rc = process_desc(virtiodata, &vr, &vrcpy, todo_items, first, id);
	if (rc < 0)
		goto recover;

	/*
	 * Now userspace can use the buffer already mapped
	 */
	memcpy(vr.avail, vrcpy.avail,
	       (copy_len - (sizeof(struct vring_desc) * num)));

	return 0;

recover:
	return -1;
}

/*
 * Copy the mapped data from the shared buffer ritgh to the client buffers
 * pointed by cur->tce
 */
int kick_callback(struct virtio_vio_device *virtiodata,
			 int num_processed, unsigned int id)
{

	struct vring rng, rngcpy, local_vring_tks, remote_vring_tks;
	char *server_ring, *databuffer;
	struct vring_avail *av;
	struct vring_used *us;
	struct vring_desc *desc, *cur;
	int i, num = 0, rc = 0, used_len = 0, start, save;
	struct vv_ring_info *vrinf, *n;
	unsigned int *queue = NULL, qlen = 0;
	dma_addr_t queue_token = 0, server_ring_tk, databuffer_tk;


	server_ring = (char *)virtiodata->outbuffer + RING_OFFSET;
	server_ring_tk = virtiodata->outbuffer_token + RING_OFFSET;
	databuffer_tk = virtiodata->outbuffer_token + RING_DATABUF_OFFSET;
	databuffer = (char *)virtiodata->outbuffer + RING_DATABUF_OFFSET;

	list_for_each_entry_safe(vrinf, n, &virtiodata->srv_vqs, node) {
		queue_token = vrinf->queue_token;
		queue = vrinf->queue;
		qlen = vring_size(vrinf->num, VIRTIO_PCI_VRING_ALIGN);
		num = vrinf->num;
		used_len =
		    sizeof(__u16) * 2 + sizeof(struct vring_used_elem) * num;
	}

	vring_init(&rngcpy, num, virtiodata->rngcpy, VIRTIO_PCI_VRING_ALIGN);
	vring_init(&rng, num, server_ring, VIRTIO_PCI_VRING_ALIGN);
	vring_init(&local_vring_tks, num, (void *)server_ring_tk,
		   VIRTIO_PCI_VRING_ALIGN);
	vring_init(&remote_vring_tks, num, (void *)queue_token,
		   VIRTIO_PCI_VRING_ALIGN);

	av = rng.avail;
	us = rng.used;
	desc = rng.desc;

	start = (us->idx - num_processed) % rng.num;
	if (start < 0)
		start = (num_processed - 1) + start;

	for (i = start; num_processed > 0;
	     num_processed--, i = (i + 1) % rng.num) {
		cur = &desc[us->ring[i].id];
		do {

			    if ((cur->flags & VRING_DESC_F_WRITE)
			    || (cur->flags & VRING_DESC_F_HEAD)) {

				rc = h_copy_rdma(cur->len,
						 virtiodata->liobn,
						 databuffer_tk + cur->offset,
						 virtiodata->riobn,
						 cur->addr);
				if (rc != H_SUCCESS) {
					pdebug
					    ("virtio_vios: %s: ==== error requests in callback %d ====\n",
					     __func__, id);
					printk
					    ("virtio_vios: %d transferring data error tce=%llu",
					     rc,cur->addr);

					do {
						if (!
						    (cur->
						     flags & VRING_DESC_F_NEXT))
						{

							break;
						}
						cur = &desc[cur->next];
					} while (!
						 (cur->
						  flags & VRING_DESC_F_NEXT));
				}
			}

			if (!(cur->flags & VRING_DESC_F_NEXT))
				break;

			cur = &desc[cur->next];
		} while (1);

	}

	pdebug("virtio_vios: %s: ==== processing %d requests in callback %d ====\n",
		__func__,save,id);
	/*
	 * Transfer vring.used back to client
	 */
	rc = h_copy_rdma(used_len,
			 virtiodata->liobn,
			 local_vring_tks.used,
			 virtiodata->riobn, remote_vring_tks.used);
	if (rc != H_SUCCESS) {
		printk("virtio_vios: %d transferring data error\n", rc);
		return rc;
	}

	return 0;

}

void vv_handle_crq(struct vv_crq *crq, struct virtio_vio_device *virtiodata)
{

	int rc;
	struct swap_buff *swap;
	int kickfailed = 0;

	swap = (struct swap_buff *)virtiodata->outbuffer;

	switch (crq->valid) {
	case 0xC0:
		switch (crq->format) {
		case 0x01:	/* Initialization message */
			printk("virtio_vios: partner initialized\n");
			/* Send back a response */
			rc = ibmvirtio_ops->send_crq(virtiodata,
						     0xC002000000000000LL, 0);
			if (rc != 0) {
				printk
				    ("virtio_vios: unable to send init rsp\n");
			}
			virtiodata->partner_ready = 1;
			break;
		case 0x02:	/* Initialization response */

			virtiodata->partner_ready = 1;

			printk
			    ("virtio_vios: partner initialization complete\n");

			break;
		default:
			printk("virtio_vios: unknown crq message type: %d\n",
			       crq->format);
			break;
		}

		return;

	case 0xFF:		/* Hypervisor telling us the connection is closed */
		printk("virtio_vios: connection %s closed\n",
		       dev_name(virtiodata->dev));
		virtiodata->partner_ready = 0;
		swap->qemu_cmd = VV_SERVER_RESET;
		virtiodata->current_pos = (sizeof(struct swap_buff));

		wake_up_interruptible(&virtiodata->usr_space_queue);

		return;
	case 0x80:
		swap->qemu_cmd = crq->format;
		swap->in_param = crq->param;

		
		switch (crq->format) {

		case VV_GET_CONFIGS:
		case VV_GET_CONFIG_HDR:
			//Stores TCE and process after qemu-vio returns
			virtiodata->remote_buf_len = crq->IU_length;
			virtiodata->remote_ptr = crq->IU_data_ptr;
			virtiodata->current_pos = (sizeof(struct swap_buff));

			break;
		case VV_REFRESH_CONFIG_HDR:
		case VV_REFRESH_CONFIG_SPACE:
			rc = h_copy_rdma(crq->IU_length,
					 virtiodata->riobn,
					 crq->IU_data_ptr,
					 virtiodata->liobn,
					 virtiodata->outbuffer_token +
					 sizeof(struct swap_buff));

			if (rc != H_SUCCESS) {
				printk
				    ("virtio_vios: %d transferring data error\n",
				     rc);
				break;
			}

			swap->len = crq->IU_length;
			virtiodata->current_pos =
			    sizeof(struct swap_buff) + crq->IU_length;

			break;
		case VV_FIND_VQ:
			if (!server_add_vq(virtiodata, crq)) {
				swap->qemu_cmd = VV_FIND_VQ;
				swap->in_param = crq->IU_length;
				virtiodata->current_pos =
				    sizeof(struct swap_buff);

			} else {
				send_ack(virtiodata, VV_FIND_VQ_RSP_FAIL, 0);
			}

			break;
		case VV_DEL_VQ:
			server_del_vq(virtiodata, crq->param);
			swap->qemu_cmd = VV_DEL_VQ;
			virtiodata->current_pos = sizeof(struct swap_buff);

			break;
		case VV_NOTIFY:
			pdebug
			    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: kick %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
			     crq->param);

			if (virtiodata->vring_busy) {
				send_ack(virtiodata, VV_KICK_BUSY, crq->param);
				pdebug
				    ("--------------------kick %d lost--------------------\n",
				     crq->param);
				kickfailed = 1;/** TEST**/
				pdebug
				    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
				     crq->param);
				break;
			}

			kickfailed = 0;
			virtiodata->vring_busy = 1;

			rc = handle_kick(virtiodata, crq->param);
			if (rc == -2) {
				pdebug
				    ("--------------------kick %d failed--------------------\n",
				     crq->param);
				kickfailed = 1;
				break;
			}
			if (rc < 0) {
				send_ack(virtiodata, VV_KICK_ERROR, crq->param);
				pdebug
				    ("--------------------kick %d failed--------------------\n",
				     crq->param);
				kickfailed = 1;
				pdebug
				    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
				     crq->param);
				break;
			}
			virtiodata->current_pos = sizeof(struct swap_buff);
			swap->in_param = crq->param;
			swap->qemu_cmd = VV_RING_KICK;

			pdebug
			    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: kick %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
			     crq->param);

			break;
		default:
			printk("virtio_vios: unknown crq message type: %d\n",
			       crq->format);
			break;
		}

		if (!kickfailed)
			wake_up_interruptible(&virtiodata->usr_space_queue);

		break;
	default:
		printk("virtio_vios: Invalid CRQ\n");
		return;
	}
	return;
}

static int release_databuf(struct virtio_vio_device *virtiodata)
{
	unsigned long virt_addr;

	dma_free_coherent(virtiodata->dev,
			  RING_MAX_LEN,
			  virtiodata->rngcpy, virtiodata->rngcpy_token);

	for (virt_addr = (unsigned long)virtiodata->outbuffer;
	     virt_addr < (unsigned long)virtiodata->outbuffer + SWAP_SIZE;
	     virt_addr += PAGE_SIZE) {
		// clear all  remapable pages
		ClearPageReserved(virt_to_page(virt_addr));
	}

	dma_free_coherent(virtiodata->dev,
			  SWAP_SIZE,
			  virtiodata->outbuffer, virtiodata->outbuffer_token);

	return 0;
}

static int init_databuf(struct virtio_vio_device *virtiodata)
{
	unsigned long virt_addr;

	virtiodata->outbuffer = dma_alloc_coherent(virtiodata->dev,
						   SWAP_SIZE,
						   &virtiodata->outbuffer_token,
						   GFP_KERNEL);
	if (!virtiodata->outbuffer) {
		return -ENOMEM;
	}

	printk("virtio_vios: allocated %d bytes as shared buffer\n", SWAP_SIZE);
	for (virt_addr = (unsigned long)virtiodata->outbuffer;
	     virt_addr < (unsigned long)virtiodata->outbuffer + SWAP_SIZE;
	     virt_addr += PAGE_SIZE) {
		//reserve all pages to make them remapable
		SetPageReserved(virt_to_page(virt_addr));
	}

	virtiodata->rngcpy = dma_alloc_coherent(virtiodata->dev,
						RING_MAX_LEN,
						&virtiodata->rngcpy_token,
						GFP_KERNEL);
	if (!virtiodata->rngcpy) {
		return -ENOMEM;
	}

	virtiodata->vqs_count = 0;
	virtiodata->last_proc_idx = 0;

	return 0;
}

/***********************************************************************
 * File system stuffs
 * *********************************************************************/
static int device_open(struct inode *inode, struct file *file)
{
	struct virtio_vio_device *virtiodata;


	virtiodata =
	    container_of(inode->i_cdev, struct virtio_vio_device, cdev);
	file->private_data = virtiodata;

	return 0;
}


int device_ioctl(struct inode *inode, struct file *file,
			unsigned int cmd, unsigned long arg)
{

	int rc, param, function, len;
	struct virtio_vio_device *virtiodata;
	struct swap_buff *swap;

	virtiodata = file->private_data;
	swap = (struct swap_buff *)virtiodata->outbuffer;
	function = swap->qemu_rsp;
	param = swap->out_param;
	len = swap->len;

	pdebug
	    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: callback %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
	     param);
	switch (cmd) {
	/*
	 * The user sinalizes he changed the shared buffer
	 * TODO: Find apropriate ioctl numbers
	 */
	case DATAREADY:

		virtiodata->current_pos = 0;

		switch (function) {
		case VV_GET_CONFIGS_RESPONSE:
		case VV_GET_CONFIG_HDR_RESPONSE:

			rc = h_copy_rdma(virtiodata->remote_buf_len,
					 virtiodata->liobn,
					 virtiodata->outbuffer_token +
					 sizeof(struct swap_buff),
					 virtiodata->riobn,
					 virtiodata->remote_ptr);
			if (rc != H_SUCCESS) {
				printk
				    ("virtio_vios: %d transferring data error\n",
				     rc);
				break;
			}
			break;

		case VV_REFRESH_CONFIG_HDR_RESPONSE:
		case VV_REFRESH_CONFIG_SPACE_RESPONSE:
			break;
		case VV_KICK_DONE:

			if (kick_callback(virtiodata, swap->len, param))
				function = VV_KICK_ERROR;
			else
				function = VV_KICK_DONE;

			break;
		case VV_SERVER_RESET_DONE:
			server_reset(virtiodata);
			return 0;
			break;
		}
		virtiodata->vring_busy = 0;
		send_ack(virtiodata, function, param);
		break;
	}
	pdebug
	    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: callback %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
	     param);
	return 0;
}

static ssize_t device_read(struct file *file, char __user * buf, size_t count,
			   loff_t * f_pos)
{
	int ret = 0;
	struct virtio_vio_device *virtiodata;
	struct swap_buff *swap;
	virtiodata = file->private_data;
	swap = (struct swap_buff *)virtiodata->outbuffer;

	pdebug
	    ("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓: %s: swap->in_param %d :↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓\n",
	     __func__,swap->in_param);

	if(more_reqs(virtiodata)){
	    pdebug
	    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: %s: swap->in_param %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
	     __func__,swap->in_param);
	    return ret;
	}

	wait_event_interruptible(virtiodata->usr_space_queue, virtiodata->current_pos > 0);	//Always sleep

	pdebug
	    ("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑: %s: swap->in_param %d :↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑\n",
	     __func__,swap->in_param);
	return ret;
}

static int device_release(struct inode *inode, struct file *file)
{
	struct virtio_vio_device *virtiodata;

	return 0;
}

/**************************** Commom VMA ops **************************/

static void vma_open(struct vm_area_struct *vma)
{

	printk("virtio_vios: mapped virt %lx, phys %lx\n", vma->vm_start,
	       vma->vm_pgoff << PAGE_SHIFT);
}

static void vma_close(struct vm_area_struct *vma)
{

	printk("virtio_vios: %s\n", __func__);
}

struct vm_operations_struct remap_vm_ops = {
	.open = vma_open,
	.close = vma_close,
};

static int device_mmap(struct file *file, struct vm_area_struct *vma)
{
	int ret;
	struct virtio_vio_device *virtiodata;
	virtiodata = file->private_data;

	ret = remap_pfn_range(vma,
			      vma->vm_start,
			      virt_to_phys((void *)((unsigned long)virtiodata->
						    outbuffer)) >> PAGE_SHIFT,
			      vma->vm_end - vma->vm_start, PAGE_SHARED);
	if (ret) {
		return -EAGAIN;
	}

	vma->vm_ops = &remap_vm_ops;
	vma_open(vma);

	return ret;
}

struct file_operations device_fops = {

	.owner = THIS_MODULE,
	.ioctl = device_ioctl,
	.read = device_read,
	.open = device_open,
	.release = device_release,
	.mmap = device_mmap,

};

static void setup_cdev(struct virtio_vio_device *virtiodata)
{
	int err, devno, minor;

	down(&idxsem);
	minor = devindex;
	devindex++;
	up(&idxsem);

	devno = MKDEV(cdev_major, minor);

	printk("virtio_vios: adding char device %d/%d <-> %s\n", cdev_major,
	       minor, dev_name(virtiodata->dev));
	/*Print driver port string */
	cdev_init(&virtiodata->cdev, &device_fops);
	virtiodata->cdev.owner = THIS_MODULE;
	virtiodata->cdev.ops = &device_fops;
	err = cdev_add(&virtiodata->cdev, devno, 1);

	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "virtio_vios: error %d adding scull %d", err,
		       devindex);
}

static int ibmvirtio_probe(struct vio_dev *vdev, const struct vio_device_id *id)
{
	int rc;
	unsigned int *dma, dma_size;
	struct virtio_vio_device *virtiodata;

	virtiodata = kmalloc(sizeof(struct virtio_vio_device), GFP_KERNEL);
	if (!virtiodata) {
		printk("virtio_vios: can't get a free page\n");
		return -ENOMEM;
	}
	memset(virtiodata, 0x00, sizeof(struct virtio_vio_device));

	virtiodata->dev = &vdev->dev;
	virtiodata->partner_ready = 0;
	INIT_LIST_HEAD(&virtiodata->srv_vqs);
	spin_lock_init(&virtiodata->vqs_lock);
	spin_lock_init(&virtiodata->vring_lock);
	virtiodata->vring_busy = 0;

	//Alloc swap buff
	rc = init_databuf(virtiodata);
	if (rc != 0) {
		printk("virtio_vios: cant alloc swap buffer\n");
	}
	//Get attributes from firmware
	dma = (unsigned int *)vio_get_attribute(vdev, "ibm,my-dma-window",
						&dma_size);
	if (!dma || dma_size != 40) {
		printk("virtio_vios: couldn't get window property %d\n",
		       dma_size);
		return -1;
	}
	virtiodata->liobn = dma[0];
	virtiodata->riobn = dma[5];

	setup_cdev(virtiodata);

	init_waitqueue_head(&virtiodata->usr_space_queue);

	rc = ibmvirtio_ops->init_crq_queue(&virtiodata->queue, virtiodata, 0);
	if (rc != 0 && rc != H_RESOURCE) {
		dev_err(&vdev->dev, "virtio_vios: couldn't initialize crq\n");
		return -ENOMEM;
	}

	dev_set_drvdata(&vdev->dev, virtiodata);

	ibmvirtio_ops->send_crq(virtiodata, 0xC001000000000000LL, 0);

	return 0;
}

static int ibmvirtio_remove(struct vio_dev *vdev)
{
	struct virtio_vio_device *virtiodata;

	virtiodata = dev_get_drvdata(&vdev->dev);

	ibmvirtio_ops->release_crq_queue(&virtiodata->queue, virtiodata, 0);
	cdev_del(&virtiodata->cdev);
	release_databuf(virtiodata);
	kfree(virtiodata);

	return 0;
}

static struct vio_device_id ibmvirtio_device_table[] __devinitdata = {
	{"v-scsi-host", "IBM,v-scsi-host"},
	{"", ""}
};

MODULE_DEVICE_TABLE(vio, ibmvirtio_device_table);

static struct vio_driver ibmvirtio_driver = {
	.id_table = ibmvirtio_device_table,
	.probe = ibmvirtio_probe,
	.remove = ibmvirtio_remove,
	.driver = {
		   .name = "virtio_vios",
		   .owner = THIS_MODULE,
		   }
};

int __init ibmvirtio_module_init(void)
{
	int first_minor, ret;
	dev_t dev = 0;

	first_minor = 0;

	printk("virtio_vios: Init...\n");

	ret = alloc_chrdev_region(&dev, first_minor, 1, "virtio");
	cdev_major = 240 /*MAJOR(dev) */ ;

	init_MUTEX(&idxsem);

	ibmvirtio_ops = &vv_ops;
	ret = vio_register_driver(&ibmvirtio_driver);
	if (ret != 0)
		return ret;

	return 0;
}

void __exit ibmvirtio_module_exit(void)
{
	unregister_chrdev_region(cdev_major, 1);
	vio_unregister_driver(&ibmvirtio_driver);
	printk("virtio_vios: ...exit.\n");
}

module_init(ibmvirtio_module_init);
module_exit(ibmvirtio_module_exit);
