/*
 * Virtio VIO driver
 *
 * This module allows virtio devices to be used over a virtual VIO device.
 * This can be used with PowerVM based VMMs
 *
 * Copyright IBM Corp. 2010
 *
 * Authors:
 *  Erlon R. Cruz <erlon.cruz@br.flextronics.com>
 *  Pedro Scarappicha <pedro.scarappicha@br.flextronics.com>
 *  Ricardo M. Matinata  <matinata@br.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2 or later.
 * See the COPYING file in the top-level directory.
 *
 */

#ifndef VIRTIOVIO_H
#define VIRTIOVIO_H
#include <linux/types.h>
#include <linux/list.h>
#include <linux/completion.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/semaphore.h>
#include <linux/cdev.h>
#include <linux/virtio.h>
#include <linux/virtio_vio.h>

/**
 * Debug vars
 */
//#define VV_DEBUG
#ifdef VV_DEBUG
#define pdebug(fmt, ...) printk(fmt,##__VA_ARGS__)
#else
#define pdebug(fmt, ...) ({ if (0) printk(fmt, ##__VA_ARGS__); 0; })
#endif

/**
 * Server/Client structures
 */

#define MAX_DEVICE_RINGS 10

struct vv_crq {
	u8 valid;		/* used by PHYP */
	u8 format;		/*CRQ format */
	u32 param;		/* parameter that will be sent */
	u16 IU_length;		/* in bytes */
	u64 IU_data_ptr;	/* the TCE for transferring data */
} __attribute__ ((packed));

struct crq_queue {
	struct vv_crq *msgs;
	int size, cur;
	dma_addr_t msg_token;
	spinlock_t lock;
};

/* all driver data associated with a host adapter */
struct virtio_vio_device {
	/* Client and server */
	struct device *dev;
	struct crq_queue queue;
	int response_received;
	int partner_ready;

	/* Client */
	unsigned int kick_sent;
	struct virtio_data {
		/*The device header and configs shared and synced with the server */
		struct vv_device_header *devhdr;
		dma_addr_t devhdr_token;
		union vv_device_config *devcfg;
		dma_addr_t devcfg_token;

		struct virtio_device vd;
		struct list_head vqs_infos;
		spinlock_t lock;
		struct list_head virtqueues;
	} vdata;
        wait_queue_head_t sender_queue;	//viortio_vioc sleeps here after asking for init headers

	/*Server */
	struct tasklet_struct vios_task;
	char *rngcpy;
	dma_addr_t rngcpy_token;
	unsigned long liobn;
	unsigned long riobn;
	struct cdev cdev;
	char *outbuffer;                //Shared buffer between kernel and userspace
	dma_addr_t outbuffer_token;	//Shared buffer's TCE
	unsigned int current_pos;	//Command bytes wrote in the buffer
        u64 remote_ptr;         	//TCE received from client holding client configs adds
	int remote_buf_len;             //client config add len
	struct list_head srv_vqs;
	int vqs_count;		//Current number of vqs in server
	int vring_busy;		//Assures nobody touches vring while uspace is processing it
	int last_proc_idx;
	spinlock_t vring_lock;
	spinlock_t vqs_lock;

	wait_queue_head_t usr_space_queue;	//usr space task sleep here when polling
};

struct vv_ring_info {

	/* the list node for the virtqueues list */
	struct list_head node;
	/* the actual virtqueue */
	struct virtqueue *vq;

	/* the number of entries in the queue */
	int num;

	/* the index of the queue */
	int queue_index;

	/* the virtual address of the ring queue */
	void *queue;
	dma_addr_t queue_token;
};

struct cdev_index {
	dev_t devnum;
	char device_iobn[15];
};

/* routines for managing a command/response queue */
void vv_handle_crq(struct vv_crq *crq, struct virtio_vio_device *hostdata);

struct ibmvirtio_ops {
	int (*init_crq_queue) (struct crq_queue * queue,
			       struct virtio_vio_device * hostdata,
			       int max_requests);
	void (*release_crq_queue) (struct crq_queue * queue,
				   struct virtio_vio_device * hostdata,
				   int max_requests);
	int (*reset_crq_queue) (struct crq_queue * queue,
				struct virtio_vio_device * hostdata);
	int (*reenable_crq_queue) (struct crq_queue * queue,
				   struct virtio_vio_device * hostdata);
	int (*send_crq) (struct virtio_vio_device * hostdata,
			 u64 word1, u64 word2);
	int (*resume) (struct virtio_vio_device * hostdata);
};

extern struct ibmvirtio_ops vv_ops;

#endif /* VIRTIOVIO_H */
